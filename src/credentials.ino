/** Load WLAN credentials from EEPROM */
bool loadCredentials() {
        EEPROM.begin(512);
        EEPROM.get(0, ssid);
        EEPROM.get(0+sizeof(ssid), password);
        char ok[2+1];
        EEPROM.get(0+sizeof(ssid)+sizeof(password), ok);
        EEPROM.end();

        if (String(ok) != String("OK")) {
                ssid[0] = 0;
                password[0] = 0;
        }

        Serial.println("Recovered credentials:");
        Serial.println(strlen(ssid)>0 ? ssid : "<no ssid>");
        Serial.println(strlen(password)>0 ? password : "<no password>");

        return strlen(ssid) > 0;
}

/** Store WLAN credentials to EEPROM */
void saveCredentials() {
        EEPROM.begin(512);
        EEPROM.put(0, ssid);
        EEPROM.put(0+sizeof(ssid), password);
        char ok[2+1] = "OK";
        EEPROM.put(0+sizeof(ssid)+sizeof(password), ok);
        EEPROM.commit();
        EEPROM.end();
}

void clearCredentials() {
        ssid[0] = 0;
        password[0] = 0;
}



void clearAndSaveCredentials() {
        clearCredentials();
        saveCredentials();
}
