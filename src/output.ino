String currentValue = "off";

void setState(String value) {
        currentValue = value;

        if (value == "on") {
                digitalWrite(LEDPIN, LOW);
                digitalWrite(RELAYPIN, LOW);
        }
        else if (value == "off") {
                digitalWrite(LEDPIN, HIGH);
                digitalWrite(RELAYPIN, HIGH);
        }

}

void initState() {
        setState(currentValue);
}

String getState() {
        return currentValue;
}


void toggleState() {
    setState(currentValue == "on" ? "off" : "on");
}
