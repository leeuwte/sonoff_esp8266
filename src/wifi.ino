void connectWifi() {
        Serial.println("Trying to connect to wifi.");


        WiFi.begin(ssid, password);

        while (WiFi.status() != WL_CONNECTED)
        {
                delay(500);
                Serial.print(".");
        }

        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());

        delay(1000);
}


void startAP() {
        /* You can add the password parameter if you want the AP to be password protected */
        WiFi.softAP(ap_ssid); // Wifi.softAP(ssid, password)

        IPAddress myIP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(myIP);

}

void getMAC() {
        uint8_t MAC_array[6];
        char MAC_char[12];

        WiFi.macAddress(MAC_array);
        for (int i = 0; i < sizeof(MAC_array); ++i) {
                sprintf(MAC_char,"%s%02x",MAC_char,MAC_array[i]);
        }

        Serial.print("MAC: ");
        Serial.println(MAC_char);
}
