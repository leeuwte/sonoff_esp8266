/*
   1MB flash sizee

   sonoff header
   1 - vcc 3v3
   2 - rx
   3 - tx
   4 - gnd
   5 - gpio 14

   esp8266 connections
   gpio  0 - button
   gpio 12 - relay
   gpio 13 - green led - active low
   gpio 14 - pin 5 on header

 */
/*
 * ESP8266 Webserver
 * Starts the ESP8266 as an access point and provides a web interface to configure WiFi credentials and control GPIO pins
 * Go to http://192.168.4.1 in a web browser connected to this access point to see it
 *
 * Created by: Ashish Derhgawen
 * E-mail: ashish.derhgawen@gmail.com
 * Blog: http://ashishrd.blogspot.com
 * Last modified: 13 September, 2015
 */


//BDL: Example https://github.com/esp8266/Arduino/blob/master/libraries/DNSServer/examples/CaptivePortalAdvanced/handleHttp.ino
#include "settings.h"

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

// Access point credentials
const char *ap_ssid = "SonoffAP";
const char *ap_password = "thereisnospoon";

/* Don't set this wifi credentials. They are configurated at runtime and stored on EEPROM */
char ssid[32] = "";
char password[32] = "";


// ESP8266 GPIO pins

#define BUTTONPIN 0
#define RELAYPIN 12
#define LEDPIN 13

ESP8266WebServer server(80);

WiFiClient espClient;

void setup()
{
        pinMode(BUTTONPIN, INPUT);
        pinMode(RELAYPIN, OUTPUT);
        pinMode(LEDPIN, OUTPUT);
        // digitalWrite(LEDPIN, LOW);
        initState();

        Serial.begin(115200);
        Serial.println();
        Serial.print("Configuring access point...");

        if (loadCredentials() ) {
                getMAC();
                connectWifi();
                initMqtt();
        } else {
                startAP();
        }


        /* Set page handler functions */
        server.on("/",   rootPageHandler);
        server.on("/wlan_config", wlanPageHandler);
        // server.on("/gpio", gpioPageHandler);

        server.on("/state", []() {
                String value = server.arg("value");

                if (value.length() > 0) {
                        setState(value);
                }

                server.send(200, "text/plain", getState() );
        });

        server.on("/toggle", []() {
                toggleState();
                server.send(200, "text/plain", getState() );
        });

        server.onNotFound(handleNotFound);

        server.begin();

        Serial.println("HTTP server started");
}

void loop()
{
        server.handleClient();

        checkMqtt();
}

/* Root page for the webserver */
void rootPageHandler()
{
        String response_message = "<html><head><title>ESP8266 Webserver</title></head>";
        response_message += "<body><h1><center>ESP8266 Webserver</center></h1>";

        if (WiFi.status() == WL_CONNECTED)
        {
                response_message += "<center>WLAN Status: Connected</center><br>";
        }
        else
        {
                response_message += "<center>WLAN Status: Disconnected</center><br>";
        }

        response_message += "<ul><li><a href='/wlan_config'>Configure WLAN settings</a></li>";
        response_message += "<li><a href='/gpio'>Control GPIO pins</h4></li></ul>";
        response_message += "</body></html>";

        server.send(200, "text/html", response_message);
}
