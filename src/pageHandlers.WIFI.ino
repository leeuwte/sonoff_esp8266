/* WLAN page allows users to set the WiFi credentials */
void wlanPageHandler()
{

        // Check if there are any GET parameters
        if (server.hasArg("ssid") && server.hasArg("password"))
        {
                //Store ssid and password in variabled
                server.arg("ssid").toCharArray(ssid, sizeof(ssid) - 1);
                server.arg("password").toCharArray(password, sizeof(password) - 1);
                saveCredentials();

                server.send(200, "text/html", "OK, restarting");
                ESP.restart();

                // connectWifi();
        }

        String response_message = "";
        response_message += "<html>";
        response_message += "<head><title>ESP8266 Webserver</title></head>";
        response_message += "<body style='background-color:PaleGoldenRod'><h1><center>WLAN Settings</center></h1>";

        if (WiFi.status() == WL_CONNECTED)
        {
                response_message += "Status: Connected<br>";
        }
        else
        {
                response_message += "Status: Disconnected<br>";
        }

        response_message += "<p>To connect to a WiFi network, please select a network...</p>";

// Get number of visible access points
        int ap_count = WiFi.scanNetworks();

        if (ap_count == 0)
        {
                response_message += "No access points found.<br>";
        }
        else
        {
                response_message += "<form method='get'>";

                // Show access points
                for (uint8_t ap_idx = 0; ap_idx < ap_count; ap_idx++)
                {
                        response_message += "<input type='radio' name='ssid' value='" + String(WiFi.SSID(ap_idx)) + "'>";
                        response_message += String(WiFi.SSID(ap_idx)) + " (RSSI: " + WiFi.RSSI(ap_idx) +")";
                        (WiFi.encryptionType(ap_idx) == ENC_TYPE_NONE) ? response_message += " " : response_message += "*";
                        response_message += "<br><br>";
                }

                response_message += "WiFi password (if required):<br>";
                response_message += "<input type='text' name='password'><br>";
                response_message += "<input type='submit' value='Connect'>";
                response_message += "</form>";
        }

        response_message += "</body></html>";

        server.send(200, "text/html", response_message);
}
