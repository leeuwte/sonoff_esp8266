#include <PubSubClient.h>

// String MQTT_DEVICE_NAME;


PubSubClient mqttClient(espClient);

void callback(char* topic, byte* payload, unsigned int length) {
        // handle message arrived

        // byte* data = (byte*)malloc(length);
        // memcpy(data,payload,length);
        // free(data);

        const char data[length];

        for (int i=0; i<length; i++) {
                data[i] = (char)payload[i];
        }

        Serial.println(length);
        Serial.println(topic);
        Serial.println(data);
}

bool isInit = false;

const char* mqtt_server = "xxx.xxx.xxx";
String MQTT_DEVICE_NAME;

void initMqtt() {

        mqttClient.setServer(mqtt_server, 1883);
        mqttClient.setCallback(callback);

        MQTT_DEVICE_NAME = WiFi.macAddress();

        isInit = true;
}
// PubSubClient client(server, 1883, callback, ethClient);

void connectMqtt() {
        if (!isInit) {
                return;
        }

        // Loop until we're reconnected
        while (!mqttClient.connected()) {
                Serial.println("Attempting MQTT connection...");
                // Serial.println(MQTT_CLIENT_ID);
                // Attempt to connect
                if (mqttClient.connect(MQTT_DEVICE_NAME.c_str(), "user", "password")) {

                        mqttClient.publish("sonoff/init", MQTT_DEVICE_NAME.c_str());

                        String topic = String("sonoff/device/") + String(MQTT_DEVICE_NAME);

                        Serial.println("----------------------------------------");
                        Serial.println(topic);
                        Serial.println("----------------------------------------");

                        mqttClient.subscribe(topic.c_str());

                        // client.subscribe("sonoff/device/" + MQTT_DEVICE_NAME.c_str());
                        Serial.println("MQTT connected");
                } else {

                        Serial.print("failed, rc=");
                        Serial.print(mqttClient.state());
                        Serial.println(" try again in 5 seconds");

//TODO: refactor for non-blocking
                        // Wait 5 seconds before retrying
                        delay(5000);
                }
        }
}

void checkMqtt() {

        connectMqtt();
        mqttClient.loop();

}
